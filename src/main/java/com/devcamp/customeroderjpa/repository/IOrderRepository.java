package com.devcamp.customeroderjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customeroderjpa.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long>{
    
}
