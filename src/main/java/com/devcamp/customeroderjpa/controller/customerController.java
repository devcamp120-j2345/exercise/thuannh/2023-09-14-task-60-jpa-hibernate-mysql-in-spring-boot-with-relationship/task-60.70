package com.devcamp.customeroderjpa.controller;



import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeroderjpa.model.CCustomer;
import com.devcamp.customeroderjpa.model.COrder;
import com.devcamp.customeroderjpa.repository.ICustomerRepository;


@CrossOrigin
@RequestMapping("/")
@RestController
public class customerController {

    @Autowired
    ICustomerRepository customerRepository;

    @GetMapping("/devcamp-customers")
    public ResponseEntity<List<CCustomer>> getAllCustomers() {
        try {
            List<CCustomer> customers = new ArrayList<>();
            customerRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

   @GetMapping("/devcamp-orders")
    public ResponseEntity<List<COrder>> getOrderByCustomerId(@RequestParam(value="customerId") long customerId) {
        try {
            CCustomer customers = customerRepository.findOrderByCustomerId(customerId);
            if (customers != null) {
                List<COrder> order = new ArrayList<>();
                order.addAll(customers.getOrders());
                return new ResponseEntity<>(order, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
