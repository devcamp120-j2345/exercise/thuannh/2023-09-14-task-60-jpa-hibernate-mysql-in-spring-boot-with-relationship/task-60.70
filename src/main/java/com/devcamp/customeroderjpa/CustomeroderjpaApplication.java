package com.devcamp.customeroderjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomeroderjpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomeroderjpaApplication.class, args);
	}

}
