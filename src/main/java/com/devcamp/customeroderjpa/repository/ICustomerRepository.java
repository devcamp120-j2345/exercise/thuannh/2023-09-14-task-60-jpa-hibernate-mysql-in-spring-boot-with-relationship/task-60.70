package com.devcamp.customeroderjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customeroderjpa.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long>{
    CCustomer findOrderByCustomerId (long customerId);
}
